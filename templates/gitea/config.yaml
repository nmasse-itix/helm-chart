apiVersion: v1
kind: Secret
metadata:
  name: {{ include "gitea.fullname" . }}
  labels:
    {{- include "gitea.labels" . | nindent 4 }}
type: Opaque
stringData:
  config_environment.sh: |-
    #!/usr/bin/env bash
    set -euo pipefail

    {{- if not (hasKey .Values.gitea.config "cache") -}}
    {{- $_ := set .Values.gitea.config "cache" dict -}}
    {{- end -}}

    {{- if not (hasKey .Values.gitea.config "server") -}}
    {{- $_ := set .Values.gitea.config "server" dict -}}
    {{- end -}}

    {{- if not (hasKey .Values.gitea.config "metrics") -}}
    {{- $_ := set .Values.gitea.config "metrics" dict -}}
    {{- end -}}

    {{- if not (hasKey .Values.gitea.config "database") -}}
    {{- $_ := set .Values.gitea.config "database" dict -}}
    {{- end -}}

    {{- if not (hasKey .Values.gitea.config "security") -}}
    {{- $_ := set .Values.gitea.config "security" dict -}}
    {{- end -}}

    {{- if not .Values.gitea.config.repository -}}
    {{- $_ := set .Values.gitea.config "repository" dict -}}
    {{- end -}}

    {{- if not (hasKey .Values.gitea.config "oauth2") -}}
    {{- $_ := set .Values.gitea.config "oauth2" dict -}}
    {{- end -}}

    {{- /* repository default settings */ -}}
    {{- if not .Values.gitea.config.repository.ROOT -}}
    {{- $_ := set .Values.gitea.config.repository "ROOT" "/data/git/gitea-repositories" -}}
    {{- end -}}

    {{- /* security default settings */ -}}
    {{- if not .Values.gitea.config.security.INSTALL_LOCK -}}
    {{- $_ := set .Values.gitea.config.security "INSTALL_LOCK" "true" -}}
    {{- end -}}

    {{- /* server default settings */ -}}
    {{- if not (hasKey .Values.gitea.config.server "HTTP_PORT") -}}
    {{- $_ := set .Values.gitea.config.server "HTTP_PORT" .Values.service.http.port -}}
    {{- end -}}
    {{- if not .Values.gitea.config.server.PROTOCOL -}}
    {{- $_ := set .Values.gitea.config.server "PROTOCOL" "http" -}}
    {{- end -}}
    {{- if not (.Values.gitea.config.server.DOMAIN) -}}
    {{- if gt (len .Values.ingress.hosts) 0 -}}
    {{- $_ := set .Values.gitea.config.server "DOMAIN" (index .Values.ingress.hosts 0).host -}}
    {{- else -}}
    {{- $_ := set .Values.gitea.config.server "DOMAIN" (include "gitea.default_domain" .) -}}
    {{- end -}}
    {{- end -}}
    {{- if not .Values.gitea.config.server.ROOT_URL -}}
    {{- if .Values.ingress.enabled -}}
    {{- if gt (len .Values.ingress.tls) 0 -}}
    {{- $_ := set .Values.gitea.config.server "ROOT_URL" (printf "%s://%s" .Values.gitea.config.server.PROTOCOL (index (index .Values.ingress.tls 0).hosts 0)) -}}
    {{- else -}}
    {{- $_ := set .Values.gitea.config.server "ROOT_URL" (printf "%s://%s" .Values.gitea.config.server.PROTOCOL (index .Values.ingress.hosts 0).host) -}}
    {{- end -}}
    {{- else -}}
    {{- $_ := set .Values.gitea.config.server "ROOT_URL" (printf "%s://%s" .Values.gitea.config.server.PROTOCOL .Values.gitea.config.server.DOMAIN) -}}
    {{- end -}}
    {{- end -}}
    {{- if not .Values.gitea.config.server.SSH_DOMAIN -}}
    {{- $_ := set .Values.gitea.config.server "SSH_DOMAIN" .Values.gitea.config.server.DOMAIN -}}
    {{- end -}}
    {{- if not .Values.gitea.config.server.SSH_PORT -}}
    {{- $_ := set .Values.gitea.config.server "SSH_PORT" .Values.service.ssh.port -}}
    {{- end -}}
    {{- if not (hasKey .Values.gitea.config.server "SSH_LISTEN_PORT") -}}
    {{- if not .Values.image.rootless -}}
    {{- $_ := set .Values.gitea.config.server "SSH_LISTEN_PORT" .Values.gitea.config.server.SSH_PORT -}}
    {{- else -}}
    {{- $_ := set .Values.gitea.config.server "SSH_LISTEN_PORT" "2222" -}}
    {{- end -}}
    {{- end -}}
    {{- if not (hasKey .Values.gitea.config.server "START_SSH_SERVER") -}}
    {{- if .Values.image.rootless -}}
    {{- $_ := set .Values.gitea.config.server "START_SSH_SERVER" "true" -}}
    {{- end -}}
    {{- end -}}
    {{- if not (hasKey .Values.gitea.config.server "APP_DATA_PATH") -}}
    {{- $_ := set .Values.gitea.config.server "APP_DATA_PATH" "/data" -}}
    {{- end -}}
    {{- if not (hasKey .Values.gitea.config.server "ENABLE_PPROF") -}}
    {{- $_ := set .Values.gitea.config.server "ENABLE_PPROF" false -}}
    {{- end -}}

    {{- /* metrics default settings */ -}}
    {{- if not (hasKey .Values.gitea.config.metrics "ENABLED") -}}
    {{- $_ := set .Values.gitea.config.metrics "ENABLED" .Values.gitea.metrics.enabled -}}
    {{- end -}}

    {{- /* database default settings */ -}}
    {{- if .Values.gitea.database.builtIn.postgresql.enabled -}}
    {{- $_ := set .Values.gitea.config.database "DB_TYPE"   "postgres" -}}
    {{- if not (.Values.gitea.config.database.HOST) -}}
    {{- $_ := set .Values.gitea.config.database "HOST"      (include "postgresql.dns" .) -}}
    {{- end -}}
    {{- $_ := set .Values.gitea.config.database "NAME"      .Values.postgresql.global.postgresql.postgresqlDatabase -}}
    {{- $_ := set .Values.gitea.config.database "USER"      .Values.postgresql.global.postgresql.postgresqlUsername -}}
    {{- $_ := set .Values.gitea.config.database "PASSWD"    .Values.postgresql.global.postgresql.postgresqlPassword -}}
    {{ else if .Values.gitea.database.builtIn.mysql.enabled -}}
    {{- $_ := set .Values.gitea.config.database "DB_TYPE"   "mysql" -}}
    {{- if not (.Values.gitea.config.database.HOST) -}}
    {{- $_ := set .Values.gitea.config.database "HOST"      (include "mysql.dns" .) -}}
    {{- end -}}
    {{- $_ := set .Values.gitea.config.database "NAME"      .Values.mysql.db.name -}}
    {{- $_ := set .Values.gitea.config.database "USER"      .Values.mysql.db.user -}}
    {{- $_ := set .Values.gitea.config.database "PASSWD"    .Values.mysql.db.password -}}
    {{ else if .Values.gitea.database.builtIn.mariadb.enabled -}}
    {{- $_ := set .Values.gitea.config.database "DB_TYPE"   "mysql" -}}
    {{- if not (.Values.gitea.config.database.HOST) -}}
    {{- $_ := set .Values.gitea.config.database "HOST"      (include "mariadb.dns" .) -}}
    {{- end -}}
    {{- $_ := set .Values.gitea.config.database "NAME"      .Values.mariadb.auth.database -}}
    {{- $_ := set .Values.gitea.config.database "USER"      .Values.mariadb.auth.username -}}
    {{- $_ := set .Values.gitea.config.database "PASSWD"    .Values.mariadb.auth.password -}}
    {{- end -}}

    {{- /* cache default settings */ -}}
    {{- if .Values.gitea.cache.builtIn.enabled -}}
    {{- $_ := set .Values.gitea.config.cache "ENABLED" "true" -}}
    {{- $_ := set .Values.gitea.config.cache "ADAPTER" "memcache" -}}
    {{- if not (.Values.gitea.config.cache.HOST) -}}
    {{- $_ := set .Values.gitea.config.cache "HOST" (include "memcached.dns" .) -}}
    {{- end -}}
    {{- end -}}

    {{- if not (hasKey .Values.gitea.config.security "INTERNAL_TOKEN") }}
    export ENV_TO_INI__SECURITY__INTERNAL_TOKEN=$(gitea generate secret INTERNAL_TOKEN)
    {{- end }}
    {{- if not (hasKey .Values.gitea.config.security "SECRET_KEY") }}
    export ENV_TO_INI__SECURITY__SECRET_KEY=$(gitea generate secret SECRET_KEY)
    {{- end }}
    {{- if not (hasKey .Values.gitea.config.oauth2 "JWT_SECRET") }}
    export ENV_TO_INI__OAUTH2__JWT_SECRET=$(gitea generate secret JWT_SECRET)
    {{- end }}

    {{- /* autogenerate app.ini environment values */ -}}
    {{- range $key, $value := .Values.gitea.config  }}
    {{- if kindIs "map" $value }}
    {{- if gt (len $value) 0 }}
    {{- range $n_key, $n_value := $value }}
    export ENV_TO_INI__{{ $key | upper | replace "." "_0X2E_" | replace "-" "_0X2D_" }}__{{ $n_key | upper }}={{ $n_value }}
    {{- end }}
    {{- end }}
    {{- else }}
    export ENV_TO_INI__{{ $key | upper | replace "." "_0X2E_" | replace "-" "_0X2D_" }}__{{ $key | upper }}={{ $value }}
    {{- end }}
    {{- end }}

    # safety to prevent rewrite of secret keys if an app.ini already exists
    if [ -f ${GITEA_APP_INI} ]; then
      unset ENV_TO_INI__SECURITY__INTERNAL_TOKEN
      unset ENV_TO_INI__SECURITY__SECRET_KEY
      unset ENV_TO_INI__OAUTH2__JWT_SECRET
    fi

    environment-to-ini -o $GITEA_APP_INI -p ENV_TO_INI
